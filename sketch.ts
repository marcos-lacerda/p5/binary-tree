/// <reference path="node_modules/@types/p5/global.d.ts" />

const valores = [];

class Item {
    constructor(public value: number) {
        btree.count++;
    }
    left: Item;
    right: Item;

    add(value: number) {
        if (value < this.value) {
            if (this.left == null) {
                this.left = new Item(value);
            } else {
                this.left.add(value);
            }
        } else if (value > this.value) {
            if (this.right == null) {
                this.right = new Item(value);

            } else {
                this.right.add(value);
            }
        }
    }

    traverse() {
        this.left && this.left.traverse();
        valores.push(this.value);
        this.right && this.right.traverse();
    }

    print(x: number, y: number) {
        let ri = this.left ? this.left.countRightItems() : 0;
        let li = this.right ? this.right.countLeftItems() : 0;
        const offset = ri+li;
        if (this.left) {
            const xl = x - (offset + 1) * 50;
            line(x-15, y-5, xl+10, y + 25);
            this.left.print(xl, y + 50);
            text(ri, x-40, y);
        }
        if (this.right) {
            const xr = x + (offset + 1) * 50;
            line(x+27, y-5, xr, y + 26);
            this.right.print(xr, y + 50);
            text(li, x+40, y);
        }
        circle(x + 7 ,y - 5, 40);
        text(this.value, x, y);
    }
    countLeftItems(): number {
        if (this.left == null) return 0;
        if (this.right) {
            return max(
                1 + this.left.countLeftItems(),
                1 - this.right.countLeftItems())

        } else {
            return 1 + this.left.countLeftItems();
        }
    }
    countRightItems() {
        if (this.right == null) return 0;
        if (this.left) {
            return max(
                1 + this.right.countRightItems(),
                1 - this.left.countRightItems());

        } else {
            return 1 + this.right.countRightItems();
        }
    }
}

class BTree {
    count = 0;
    root: Item;
    add(value: number) {
        if (this.root == null) {
            this.root = new Item(value);
        } else {
            this.root.add(value);
        }
    }

    traverse() {
        if (this.root != null) {
            this.root.traverse();
        }
    }

    print() {
        if (this.root != null) {
            this.root.print(width / 2, 60);
        }
    }
}

const WIDTH = 2600;
const HEIGHT = 500;
const TOT_ITEMS = 40;

let btree;

function setup() {
    createCanvas(WIDTH + 50, HEIGHT + 50);
    btree = new BTree();
    btree.add(50);
    // btree.traverse();
    // console.log(valores)
}

function mouseClicked(e) {
    if (isLooping()) {
        noLoop();
    } else {
        loop();
    }
}

function draw() {
    background(255)
    noFill();
    stroke(0);
    btree.add(floor(random(100)));
    btree.print();
    text(btree.count, width / 2, 20);
    if (btree.count > TOT_ITEMS) {
        noLoop();
    }
    frameRate(1);
}
